package com.ruoyi.system.domain;

/**
 * 低代码页面对象 sys_form_page
 */
public class SysFormPage {
    private static final long serialVersionUID = 1L;

    private Long pageId;

    /**
     * $column.columnComment
     */
    private Long formId;

    /**
     * 页面名称
     */
    private String pageName;

    /**
     * 页面类型
     */
    private String pageType;

    /**
     * 表单内容
     */
    private String pageContent;

    /**
     * 状态
     */
    private String status;

    /**
     * 备注
     */
    private String remark;

    public void setPageId(Long pageId) {
        this.pageId = pageId;
    }

    public Long getPageId() {
        return pageId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public Long getFormId() {
        return formId;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageType(String pageType) {
        this.pageType = pageType;
    }

    public String getPageType() {
        return pageType;
    }

    public void setPageContent(String pageContent) {
        this.pageContent = pageContent;
    }

    public String getPageContent() {
        return pageContent;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
