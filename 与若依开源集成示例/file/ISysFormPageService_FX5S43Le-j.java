package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.SysFormPage;

/**
 * 低代码页面Service接口
 */
public interface ISysFormPageService 
{
    /**
     * 查询低代码页面
     * 
     * @param pageId 低代码页面主键
     * @return 低代码页面
     */
    public SysFormPage selectSysFormPageByPageId(Long pageId);

    /**
     * 查询低代码页面列表
     * 
     * @param sysFormPage 低代码页面
     * @return 低代码页面集合
     */
    public List<SysFormPage> selectSysFormPageList(SysFormPage sysFormPage);

    /**
     * 新增低代码页面
     * 
     * @param sysFormPage 低代码页面
     * @return 结果
     */
    public int insertSysFormPage(SysFormPage sysFormPage);

    /**
     * 修改低代码页面
     * 
     * @param sysFormPage 低代码页面
     * @return 结果
     */
    public int updateSysFormPage(SysFormPage sysFormPage);

    /**
     * 批量删除低代码页面
     * 
     * @param pageIds 需要删除的低代码页面主键集合
     * @return 结果
     */
    public int deleteSysFormPageByPageIds(Long[] pageIds);

    /**
     * 删除低代码页面信息
     * 
     * @param pageId 低代码页面主键
     * @return 结果
     */
    public int deleteSysFormPageByPageId(Long pageId);
}
