package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.SysFormPage;
import com.ruoyi.system.service.ISysFormPageService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 低代码页面Controller
 *
 * @author ruoyi
 * @date 2024-02-26
 */
@RestController
@RequestMapping("/system/page")
public class SysFormPageController extends BaseController
{
    @Autowired
    private ISysFormPageService sysFormPageService;

    /**
     * 查询低代码页面列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SysFormPage sysFormPage)
    {
        startPage();
        List<SysFormPage> list = sysFormPageService.selectSysFormPageList(sysFormPage);
        return getDataTable(list);
    }

    /**
     * 导出低代码页面列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysFormPage sysFormPage)
    {
        List<SysFormPage> list = sysFormPageService.selectSysFormPageList(sysFormPage);
        ExcelUtil<SysFormPage> util = new ExcelUtil<SysFormPage>(SysFormPage.class);
        util.exportExcel(response, list, "低代码页面数据");
    }

    /**
     * 获取低代码页面详细信息
     */
    @GetMapping(value = "/{pageId}")
    public AjaxResult getInfo(@PathVariable("pageId") Long pageId)
    {
        return success(sysFormPageService.selectSysFormPageByPageId(pageId));
    }

    /**
     * 新增低代码页面
     */
    @PostMapping
    public AjaxResult add(@RequestBody SysFormPage sysFormPage)
    {
        return toAjax(sysFormPageService.insertSysFormPage(sysFormPage));
    }

    /**
     * 修改低代码页面
     */
    @PutMapping
    public AjaxResult edit(@RequestBody SysFormPage sysFormPage)
    {
        return toAjax(sysFormPageService.updateSysFormPage(sysFormPage));
    }

    /**
     * 删除低代码页面
     */
	@DeleteMapping("/{pageIds}")
    public AjaxResult remove(@PathVariable Long[] pageIds)
    {
        return toAjax(sysFormPageService.deleteSysFormPageByPageIds(pageIds));
    }
}
