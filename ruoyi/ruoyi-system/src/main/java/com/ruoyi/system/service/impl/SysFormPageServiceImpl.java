package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysFormPageMapper;
import com.ruoyi.system.domain.SysFormPage;
import com.ruoyi.system.service.ISysFormPageService;

/**
 * 低代码页面Service业务层处理
 */
@Service
public class SysFormPageServiceImpl implements ISysFormPageService 
{
    @Autowired
    private SysFormPageMapper sysFormPageMapper;

    /**
     * 查询低代码页面
     * 
     * @param pageId 低代码页面主键
     * @return 低代码页面
     */
    @Override
    public SysFormPage selectSysFormPageByPageId(Long pageId)
    {
        return sysFormPageMapper.selectSysFormPageByPageId(pageId);
    }

    /**
     * 查询低代码页面列表
     * 
     * @param sysFormPage 低代码页面
     * @return 低代码页面
     */
    @Override
    public List<SysFormPage> selectSysFormPageList(SysFormPage sysFormPage)
    {
        return sysFormPageMapper.selectSysFormPageList(sysFormPage);
    }

    /**
     * 新增低代码页面
     * 
     * @param sysFormPage 低代码页面
     * @return 结果
     */
    @Override
    public int insertSysFormPage(SysFormPage sysFormPage)
    {
        return sysFormPageMapper.insertSysFormPage(sysFormPage);
    }

    /**
     * 修改低代码页面
     * 
     * @param sysFormPage 低代码页面
     * @return 结果
     */
    @Override
    public int updateSysFormPage(SysFormPage sysFormPage)
    {
        return sysFormPageMapper.updateSysFormPage(sysFormPage);
    }

    /**
     * 批量删除低代码页面
     * 
     * @param pageIds 需要删除的低代码页面主键
     * @return 结果
     */
    @Override
    public int deleteSysFormPageByPageIds(Long[] pageIds)
    {
        return sysFormPageMapper.deleteSysFormPageByPageIds(pageIds);
    }

    /**
     * 删除低代码页面信息
     * 
     * @param pageId 低代码页面主键
     * @return 结果
     */
    @Override
    public int deleteSysFormPageByPageId(Long pageId)
    {
        return sysFormPageMapper.deleteSysFormPageByPageId(pageId);
    }
}
